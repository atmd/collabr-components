import Feedback from './src/feedback';
import Login from './src/login';
import Products, { ProductFull } from './src/products';
import Cart from './src/cart';
import User from './src/user';
import Search from './src/search';

import React from 'react';
import reactDom from 'react-dom';

import {Button} from 'reactstrap';

const items = [
    {"sku":"P20366","brand":"5% Nutrition","name":"Kill It, Mango Pineapple - 309g","qty":"50+","flavour":"Mango Pineapple","weight":"0.38","img":"https://www.powerbody.eu/media/catalog/product/5/1/51710ee875a831418df349c4943b9d11.jpg","price":"33.53","FIELD9":""}
];

// reactDom.render(<Feedback buttonLabel='Leave some feedback' onFeedback={data => console.log('submitted', data)} />, document.getElementById('feedback'));
// reactDom.render(<Login onLogin={data => console.log('login', data)} />, document.getElementById('login'));
// reactDom.render(<Products ctaLabel='click me' ctaAction={(data) => console.log('clicked', data)} items={items} />, document.getElementById('user'));
// reactDom.render(<Cart items={ [ {name: 'some item'} ] } />, document.getElementById('cart'));

//reactDom.render(<User thumbnail name/>, document.getElementById('user'));

reactDom.render(<ProductFull item={items[0]} {...items[0]} onClick={() => {}} />, document.getElementById('search'));
