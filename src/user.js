import React, { Component } from 'react';

export default ({picture, name, nickname}) => {
    return (
        <div>
            <img width='64' style={{'borderRadius': '200px'}} src={picture} alt={nickname} />
            <p style={{display: 'inline-block', 'marginLeft': '15px'}}>{name}</p>
        </div>
    );
}
