import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, FormFeedback } from 'reactstrap';

export default class Login extends Component {

    constructor() {
        super();
        this.state = {
            email: {
                color: '',
                message: ''
            },
            password: {
                color: '',
                message: ''
            }
        };

        this.onLogin = this.onLogin.bind(this);
        this.validtion = this.validation.bind(this);
    }

    validation() {
        let email = document.getElementById('l_username').value;
        let pass = document.getElementById('l_password').value;
        // TODO validation methods for email and password

        if (email == '' && pass == '') {

            this.setState({
               email: {
                    color: 'warning',
                    message: 'email validation'
                },
                password: {
                    color: 'warning',
                    message: 'password validation'
                }
            });

            return false;
        }
        
        if (email == '' || pass == '') {

            if (email == '') {
                this.setState({
                    email: {
                        color: 'warning',
                        message: 'email validation'
                    }
                });
            } else {
                this.setState({
                    email: {
                        color: '',
                        message: ''
                    }
                });
            }

            if (pass == '') {
                this.setState({
                    password: {
                        color: 'warning',
                        message: 'password validation'
                    }
                });
            } else {
                this.setState({
                    password: {
                        color: '',
                        message: ''
                    }
                });
            }

            return false;
        }

        if (email != '' || pass != '') {
            this.setState({
               email: {
                    color: '',
                },
                password: {
                    color: ''
                }
            });
            return true;
        }     
    }

    onLogin() {
        if (this.validation()) {
            this.props.onLogin({
                email: document.getElementById('l_username').value, 
                password: document.getElementById('l_password').value
            });
        }
    }

    renderError({message}) {
        return (<FormFeedback>{message}</FormFeedback>);
    }

    render() {
            return (
                <Form>
                    <FormGroup color={this.state.email.color}>
                        <Label for="exampleEmail">Email</Label>
                        <Input state={this.state.email.color} type="email" name="email" id="l_username" placeholder="with a placeholder" />
                        { this.renderError(this.state.email) }
                    </FormGroup>

                    <FormGroup color={this.state.password.color}>
                        <Label for="examplePassword">Password</Label>
                        <Input state={this.state.password.color} type="password" name="password" id="l_password" placeholder="password placeholder" />
                        { this.renderError(this.state.password) }
                    </FormGroup>

                    <Button id='l_submit' onClick={this.onLogin}>Login</Button>
                </Form>
            );
    }
}

