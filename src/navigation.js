import React, { Component} from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { Link } from 'react-router-dom';

import Login from './login';

export default class Header extends Component {

    constructor (props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.login = this.login.bind(this);
        this.onLogin = this.onLogin.bind(this);
        this.state = {
            isOpen: false,
            modal: false
        };
    }

    toggle () {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    login() {
    this.setState({
      modal: !this.state.modal
    });
  }

  onLogin(user, pass) {
    this.setState({
      modal: false
    });

    this.props.onLogin(user, pass);
  }

    renderNavItem (item) {
        return (
            <NavItem key={item.to}>
                <NavLink><Link to={item.to}>{item.title}</Link></NavLink>
            </NavItem>
        );
    }

    renderLogin() {
        if (this.props.authorised) {
            return (
                <NavItem>
                    <NavLink>Logout</NavLink>
                </NavItem>
            );
        } else {
            return (
                <NavItem>
                    <NavLink onClick={this.login}>Login</NavLink>
                </NavItem>
            );
        }
    }
	render () {
	
		const {title} = this.props;

        return (
            <Navbar color="faded" light toggleable>
                <NavbarToggler right onClick={this.toggle} />
                <NavbarBrand href="/">{title}</NavbarBrand>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        { this.props.links.map(this.renderNavItem) }
                        { this.renderLogin() }
                    </Nav>
                </Collapse>

                <Modal isOpen={this.state.modal} toggle={this.login} className={this.props.className}>
                    <ModalHeader toggle={this.login}>Login</ModalHeader>
                    <ModalBody>
                        <Login login={this.onLogin}/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.login}>Cancel</Button>
                    </ModalFooter>
                </Modal>

            </Navbar>
        );
    }
}

