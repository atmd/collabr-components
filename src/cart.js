import React from 'react';
import { ListGroup, ListGroupItem, Badge, Button } from 'reactstrap';

export default ({items}) => {
    return (
        <ListGroup>
            { items.map(i => <ListGroupItem className="justify-content-between"><Button color="link">remove</Button>{i.name} <Badge color="primary" pill>14</Badge></ListGroupItem>) }    
        </ListGroup>
    );
}
