import React, {Component} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

    render() {
        return (
            <div>
                <Form inline>
                    <FormGroup>
                        <Label for="exampleSearch" hidden>Search</Label>
                        <Input type="search" name="search" id="exampleSearch" placeholder="search placeholder" />
                     <Button onClick={this.toggle}>Search</Button>
                    </FormGroup>
                </Form>

                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Results</ModalHeader>
                    <ModalBody>

                  </ModalBody>
                </Modal>
            </div>
        );
    }
}

