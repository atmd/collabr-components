import React, {Component} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class Feedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }


onSubmit() {
    this.props.onFeedback({
        email: document.getElementById('fb_email').value,
        message: document.getElementById('fb_message').value,
        newsletter: document.getElementById('fb_newsletter').checked
    });
    
    this.setState({
      modal: false
    });
}
    render() {
        return (
            <div>
                <Button color="danger" onClick={this.toggle}>{this.props.buttonLabel}</Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Feedback</ModalHeader>
                    <ModalBody>
                        <p>Lorem ipsum Excepteur sint ont mollit anim id est laborum.</p>
                        <Form>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input type="text" name="text" id="fb_email" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input type="textarea" name="text" id="fb_message" />
                            </FormGroup>
                                    <FormGroup check>
                            <Label check>
                                <Input type="checkbox" id='fb_newsletter' />
                                Sign me up to the news letter
                            </Label>
                            </FormGroup>
                        </Form>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={this.onSubmit}>submit</Button>{' '}
                    <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                  </ModalFooter>
                </Modal>
            </div>
        );
    }
}

