import React, { Component } from 'react';
import { Jumbotron, InputGroup, Form, InputGroupButton, InputGroupAddon, Input, Button } from 'reactstrap';


export default class Holding extends Component {


    renderNewsletter () {
        if (this.props.newsletter) {
            const site = this.props.title.toLowerCase().replace(' ', '-');

            return (
                <Form name={`${site}-newsletter`} data-netlify='true' className='c-holding__form'>

                    <p className='c-holding__form-text'>Leave your email and we'll let you know when we launch</p>

                    <InputGroup>
                        <InputGroupAddon>@</InputGroupAddon>
                        <Input placeholder="Email" />
                        <InputGroupButton><Button color="secondary">Ok</Button></InputGroupButton>
                    </InputGroup>
                </Form>
            );
        }
    }

    render () {
        return (
           <Jumbotron className='c-holding'>
            <img className='c-holding__logo' src={this.props.logo} alt="" />
            <h2 className="c-holding__title display-4">{this.props.title}</h2>
            <h3 className="c-holding__subtitle">{this.props.subtitle}</h3>

            {this.renderNewsletter()}
          </Jumbotron>
        );
    }
}

