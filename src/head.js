import React from 'react';
import {Helmet} from "react-helmet";

export default ({site, subtitle, url, description}) => {
    return (
        <Helmet>
            <title>{`${subtitle} | ${site}`}</title>
            <link rel="canonical" href={url} />
            <meta name="description" content={description} />
        </Helmet>
    );
}
