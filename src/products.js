import React, { Component } from 'react';
import { Row, Container, Card, CardImg, CardText, CardBlock, CardTitle, CardSubtitle, Button } from 'reactstrap';
import {Link} from 'react-router-dom';


const ProductFull = ({item, name, description, img, onClick, ctaLabel, ctaAction, classes}) => (
      <Card className={`col-sm-3 c-product ${classes}`} >
        <CardImg top src={img} alt="Card image cap" />
        <CardBlock>
          <CardSubtitle className='c-product__title'><Link to={`/products/${item.sku}`}>{name}</Link></CardSubtitle>
          <CardText className='c-product__description'>{description}</CardText>
          <Button className='c-product__cta' onClick={onClick.bind(item, ctaAction)}>{ctaLabel}</Button>
        </CardBlock>
      </Card>
);

const ProductInCart = () => (<p>Product</p>);

const ProductInCheckout = () => (<p>Product</p>);


export default class Products extends Component {

    onClick(action) {
        action(this);
    }
    render() {
        return (
            <Container>
                <Row>
                    {this.props.items.map(item => <ProductFull onClick={this.onClick} {...this.props} key={item.sku} {...item} item={item} />)}
                </Row>
            </Container>
        );
    }
} 

export {ProductFull};