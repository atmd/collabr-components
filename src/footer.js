import React from 'react';

export default ({site}) => {
    return (
        <footer>
            <span>{site} - {new Date().getFullYear()}</span>
        </footer>
    );
}

