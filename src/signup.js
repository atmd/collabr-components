import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default ({}) => {
    return (
       <Form>
            <FormGroup>
                <Label for="exampleEmail">Name</Label>
                <Input type="email" name="email" id="exampleEmail" placeholder="First" />
                <Input type="email" name="email" id="exampleEmail" placeholder="Surname" />
            </FormGroup>

            <FormGroup>
                <Label for="exampleEmail">Company</Label>
                <Input type="email" name="email" id="exampleEmail" placeholder="Company" />
                <FormText color="muted">You company name etc etc</FormText>
            </FormGroup>

            <FormGroup>
                <Label for="exampleEmail">Email</Label>
                <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
                <FormText color="muted">This is some placeholder</FormText>
            </FormGroup>

            <Button>Sign up</Button>
        </Form>
    );
}
