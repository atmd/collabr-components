'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

exports.default = function (_ref) {
    _objectDestructuringEmpty(_ref);

    return _react2.default.createElement(
        _reactstrap.Form,
        null,
        _react2.default.createElement(
            _reactstrap.FormGroup,
            null,
            _react2.default.createElement(
                _reactstrap.Label,
                { 'for': 'exampleEmail' },
                'Name'
            ),
            _react2.default.createElement(_reactstrap.Input, { type: 'email', name: 'email', id: 'exampleEmail', placeholder: 'First' }),
            _react2.default.createElement(_reactstrap.Input, { type: 'email', name: 'email', id: 'exampleEmail', placeholder: 'Surname' })
        ),
        _react2.default.createElement(
            _reactstrap.FormGroup,
            null,
            _react2.default.createElement(
                _reactstrap.Label,
                { 'for': 'exampleEmail' },
                'Company'
            ),
            _react2.default.createElement(_reactstrap.Input, { type: 'email', name: 'email', id: 'exampleEmail', placeholder: 'Company' }),
            _react2.default.createElement(
                _reactstrap.FormText,
                { color: 'muted' },
                'You company name etc etc'
            )
        ),
        _react2.default.createElement(
            _reactstrap.FormGroup,
            null,
            _react2.default.createElement(
                _reactstrap.Label,
                { 'for': 'exampleEmail' },
                'Email'
            ),
            _react2.default.createElement(_reactstrap.Input, { type: 'email', name: 'email', id: 'exampleEmail', placeholder: 'with a placeholder' }),
            _react2.default.createElement(
                _reactstrap.FormText,
                { color: 'muted' },
                'This is some placeholder'
            )
        ),
        _react2.default.createElement(
            _reactstrap.Button,
            null,
            'Sign up'
        )
    );
};