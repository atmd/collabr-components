'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Holding = function (_Component) {
    _inherits(Holding, _Component);

    function Holding() {
        _classCallCheck(this, Holding);

        return _possibleConstructorReturn(this, (Holding.__proto__ || Object.getPrototypeOf(Holding)).apply(this, arguments));
    }

    _createClass(Holding, [{
        key: 'renderNewsletter',
        value: function renderNewsletter() {
            if (this.props.newsletter) {
                var site = this.props.title.toLowerCase().replace(' ', '-');

                return _react2.default.createElement(
                    _reactstrap.Form,
                    { name: site + '-newsletter', 'data-netlify': 'true', className: 'c-holding__form' },
                    _react2.default.createElement(
                        'p',
                        { className: 'c-holding__form-text' },
                        'Leave your email and we\'ll let you know when we launch'
                    ),
                    _react2.default.createElement(
                        _reactstrap.InputGroup,
                        null,
                        _react2.default.createElement(
                            _reactstrap.InputGroupAddon,
                            null,
                            '@'
                        ),
                        _react2.default.createElement(_reactstrap.Input, { placeholder: 'Email' }),
                        _react2.default.createElement(
                            _reactstrap.InputGroupButton,
                            null,
                            _react2.default.createElement(
                                _reactstrap.Button,
                                { color: 'secondary' },
                                'Ok'
                            )
                        )
                    )
                );
            }
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _reactstrap.Jumbotron,
                { className: 'c-holding' },
                _react2.default.createElement('img', { className: 'c-holding__logo', src: this.props.logo, alt: '' }),
                _react2.default.createElement(
                    'h2',
                    { className: 'c-holding__title display-4' },
                    this.props.title
                ),
                _react2.default.createElement(
                    'h3',
                    { className: 'c-holding__subtitle' },
                    this.props.subtitle
                ),
                this.renderNewsletter()
            );
        }
    }]);

    return Holding;
}(_react.Component);

exports.default = Holding;