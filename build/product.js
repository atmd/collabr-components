'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProductFull = function ProductFull(_ref) {
  var name = _ref.name,
      subtitle = _ref.subtitle,
      description = _ref.description,
      buttons = _ref.buttons,
      children = _ref.children;
  return _react2.default.createElement(
    _reactstrap.Card,
    null,
    _react2.default.createElement(_reactstrap.CardImg, { top: true, width: '100%', src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180', alt: 'Card image cap' }),
    _react2.default.createElement(
      _reactstrap.CardBlock,
      null,
      _react2.default.createElement(
        _reactstrap.CardTitle,
        null,
        name
      ),
      _react2.default.createElement(
        _reactstrap.CardSubtitle,
        null,
        subtitle
      ),
      _react2.default.createElement(
        _reactstrap.CardText,
        null,
        description
      ),
      children
    )
  );
};

var ProductInCart = function ProductInCart() {
  return _react2.default.createElement(
    'p',
    null,
    'Product'
  );
};

var ProductInCheckout = function ProductInCheckout() {
  return _react2.default.createElement(
    'p',
    null,
    'Product'
  );
};

var Product = function (_Component) {
  _inherits(Product, _Component);

  function Product() {
    _classCallCheck(this, Product);

    return _possibleConstructorReturn(this, (Product.__proto__ || Object.getPrototypeOf(Product)).apply(this, arguments));
  }

  _createClass(Product, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(ProductFull, this.props);
    }
  }]);

  return Product;
}(_react.Component);

exports.default = Product;