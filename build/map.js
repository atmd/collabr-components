'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _googleMapsReact = require('google-maps-react');

var _googleMapsReact2 = _interopRequireDefault(_googleMapsReact);

var _marker = require('./marker');

var _marker2 = _interopRequireDefault(_marker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mapStyles = [{
    "featureType": "all",
    "elementType": "geometry",
    "stylers": [{
        "color": "#333333"
    }]
}, {
    "featureType": "all",
    "elementType": "labels.text.fill",
    "stylers": [{
        "gamma": 0.01
    }, {
        "lightness": 20
    }, {
        "visibility": "on"
    }, {
        "color": "#ffffff"
    }]
}, {
    "featureType": "all",
    "elementType": "labels.text.stroke",
    "stylers": [{
        "saturation": -31
    }, {
        "lightness": -33
    }, {
        "weight": "0.01"
    }, {
        "gamma": 0.8
    }]
}, {
    "featureType": "all",
    "elementType": "labels.icon",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "administrative.neighborhood",
    "elementType": "labels.text",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "administrative.neighborhood",
    "elementType": "labels.text.fill",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [{
        "lightness": 30
    }, {
        "saturation": "100"
    }, {
        "visibility": "on"
    }, {
        "color": "#444444"
    }]
}, {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [{
        "saturation": 20
    }, {
        "visibility": "off"
    }, {
        "color": "#666666"
    }]
}, {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [{
        "lightness": 20
    }, {
        "saturation": -20
    }]
}, {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [{
        "lightness": 10
    }, {
        "saturation": -30
    }]
}, {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [{
        "color": "#222222"
    }, {
        "visibility": "on"
    }]
}, {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [{
        "saturation": 25
    }, {
        "lightness": 25
    }, {
        "visibility": "off"
    }]
}, {
    "featureType": "road",
    "elementType": "labels.text",
    "stylers": [{
        "saturation": "-100"
    }, {
        "weight": "5.00"
    }]
}, {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [{
        "color": "#ffffff"
    }]
}, {
    "featureType": "road",
    "elementType": "labels.text.stroke",
    "stylers": [{
        "visibility": "on"
    }, {
        "color": "#333333"
    }]
}, {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [{
        "visibility": "on"
    }, {
        "color": "#ff9900"
    }]
}, {
    "featureType": "road.arterial",
    "elementType": "geometry.fill",
    "stylers": [{
        "visibility": "on"
    }]
}, {
    "featureType": "road.arterial",
    "elementType": "labels.text",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "water",
    "elementType": "all",
    "stylers": [{
        "lightness": -20
    }, {
        "color": "#222222"
    }]
}, {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [{
        "visibility": "off"
    }, {
        "color": "#222222"
    }]
}];

var Mapz = function (_Component) {
    _inherits(Mapz, _Component);

    function Mapz() {
        _classCallCheck(this, Mapz);

        var _this = _possibleConstructorReturn(this, (Mapz.__proto__ || Object.getPrototypeOf(Mapz)).call(this));

        _this.state = {
            activeMarker: null,
            showingInfoWindow: false
        };
        _this.clicked = _this.clicked.bind(_this);
        return _this;
    }

    _createClass(Mapz, [{
        key: 'clicked',
        value: function clicked(props, marker, e) {
            this.setState({
                activeMarker: marker,
                selectedMarker: props,
                showingInfoWindow: true
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var name = this.state.selectedMarker ? this.state.selectedMarker.name : '';

            return _react2.default.createElement(
                _googleMapsReact2.default,
                { disableDefaultUI: true, scaleControl: true, styles: mapStyles, className: 'c-map__map', google: window.google, zoom: 11, initialCenter: { lat: this.props.latitude, lng: this.props.longitude } },
                this.props.markers.map(function (m) {
                    return _react2.default.createElement(_marker2.default, { onClick: _this2.clicked, key: m.name, position: { lat: m.location.latitude, lng: m.location.longitude }, name: m.name });
                }),
                _react2.default.createElement(
                    _googleMapsReact.InfoWindow,
                    { marker: this.state.activeMarker, visible: this.state.showingInfoWindow },
                    _react2.default.createElement(
                        'div',
                        { className: 'c-infowindow' },
                        _react2.default.createElement(
                            'h1',
                            null,
                            name
                        )
                    )
                )
            );
        }
    }]);

    return Mapz;
}(_react.Component);

exports.default = Mapz;