'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Login = function (_Component) {
    _inherits(Login, _Component);

    function Login() {
        _classCallCheck(this, Login);

        var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this));

        _this.state = {
            email: {
                color: '',
                message: ''
            },
            password: {
                color: '',
                message: ''
            }
        };

        _this.onLogin = _this.onLogin.bind(_this);
        _this.validtion = _this.validation.bind(_this);
        return _this;
    }

    _createClass(Login, [{
        key: 'validation',
        value: function validation() {
            var email = document.getElementById('l_username').value;
            var pass = document.getElementById('l_password').value;
            // TODO validation methods for email and password

            if (email == '' && pass == '') {

                this.setState({
                    email: {
                        color: 'warning',
                        message: 'email validation'
                    },
                    password: {
                        color: 'warning',
                        message: 'password validation'
                    }
                });

                return false;
            }

            if (email == '' || pass == '') {

                if (email == '') {
                    this.setState({
                        email: {
                            color: 'warning',
                            message: 'email validation'
                        }
                    });
                } else {
                    this.setState({
                        email: {
                            color: '',
                            message: ''
                        }
                    });
                }

                if (pass == '') {
                    this.setState({
                        password: {
                            color: 'warning',
                            message: 'password validation'
                        }
                    });
                } else {
                    this.setState({
                        password: {
                            color: '',
                            message: ''
                        }
                    });
                }

                return false;
            }

            if (email != '' || pass != '') {
                this.setState({
                    email: {
                        color: ''
                    },
                    password: {
                        color: ''
                    }
                });
                return true;
            }
        }
    }, {
        key: 'onLogin',
        value: function onLogin() {
            if (this.validation()) {
                this.props.onLogin({
                    email: document.getElementById('l_username').value,
                    password: document.getElementById('l_password').value
                });
            }
        }
    }, {
        key: 'renderError',
        value: function renderError(_ref) {
            var message = _ref.message;

            return _react2.default.createElement(
                _reactstrap.FormFeedback,
                null,
                message
            );
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _reactstrap.Form,
                null,
                _react2.default.createElement(
                    _reactstrap.FormGroup,
                    { color: this.state.email.color },
                    _react2.default.createElement(
                        _reactstrap.Label,
                        { 'for': 'exampleEmail' },
                        'Email'
                    ),
                    _react2.default.createElement(_reactstrap.Input, { state: this.state.email.color, type: 'email', name: 'email', id: 'l_username', placeholder: 'with a placeholder' }),
                    this.renderError(this.state.email)
                ),
                _react2.default.createElement(
                    _reactstrap.FormGroup,
                    { color: this.state.password.color },
                    _react2.default.createElement(
                        _reactstrap.Label,
                        { 'for': 'examplePassword' },
                        'Password'
                    ),
                    _react2.default.createElement(_reactstrap.Input, { state: this.state.password.color, type: 'password', name: 'password', id: 'l_password', placeholder: 'password placeholder' }),
                    this.renderError(this.state.password)
                ),
                _react2.default.createElement(
                    _reactstrap.Button,
                    { id: 'l_submit', onClick: this.onLogin },
                    'Login'
                )
            );
        }
    }]);

    return Login;
}(_react.Component);

exports.default = Login;