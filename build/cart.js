'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var items = _ref.items;

    return _react2.default.createElement(
        _reactstrap.ListGroup,
        null,
        items.map(function (i) {
            return _react2.default.createElement(
                _reactstrap.ListGroupItem,
                { className: 'justify-content-between' },
                _react2.default.createElement(
                    _reactstrap.Button,
                    { color: 'link' },
                    'remove'
                ),
                i.name,
                ' ',
                _react2.default.createElement(
                    _reactstrap.Badge,
                    { color: 'primary', pill: true },
                    '14'
                )
            );
        })
    );
};