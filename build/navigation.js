'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

var _reactRouterDom = require('react-router-dom');

var _login = require('./login');

var _login2 = _interopRequireDefault(_login);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Header = function (_Component) {
    _inherits(Header, _Component);

    function Header(props) {
        _classCallCheck(this, Header);

        var _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props));

        _this.toggle = _this.toggle.bind(_this);
        _this.login = _this.login.bind(_this);
        _this.onLogin = _this.onLogin.bind(_this);
        _this.state = {
            isOpen: false,
            modal: false
        };
        return _this;
    }

    _createClass(Header, [{
        key: 'toggle',
        value: function toggle() {
            this.setState({
                isOpen: !this.state.isOpen
            });
        }
    }, {
        key: 'login',
        value: function login() {
            this.setState({
                modal: !this.state.modal
            });
        }
    }, {
        key: 'onLogin',
        value: function onLogin(user, pass) {
            this.setState({
                modal: false
            });

            this.props.onLogin(user, pass);
        }
    }, {
        key: 'renderNavItem',
        value: function renderNavItem(item) {
            return _react2.default.createElement(
                _reactstrap.NavItem,
                { key: item.to },
                _react2.default.createElement(
                    _reactstrap.NavLink,
                    null,
                    _react2.default.createElement(
                        _reactRouterDom.Link,
                        { to: item.to },
                        item.title
                    )
                )
            );
        }
    }, {
        key: 'renderLogin',
        value: function renderLogin() {
            if (this.props.authorised) {
                return _react2.default.createElement(
                    _reactstrap.NavItem,
                    null,
                    _react2.default.createElement(
                        _reactstrap.NavLink,
                        null,
                        'Logout'
                    )
                );
            } else {
                return _react2.default.createElement(
                    _reactstrap.NavItem,
                    null,
                    _react2.default.createElement(
                        _reactstrap.NavLink,
                        { onClick: this.login },
                        'Login'
                    )
                );
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var title = this.props.title;


            return _react2.default.createElement(
                _reactstrap.Navbar,
                { color: 'faded', light: true, toggleable: true },
                _react2.default.createElement(_reactstrap.NavbarToggler, { right: true, onClick: this.toggle }),
                _react2.default.createElement(
                    _reactstrap.NavbarBrand,
                    { href: '/' },
                    title
                ),
                _react2.default.createElement(
                    _reactstrap.Collapse,
                    { isOpen: this.state.isOpen, navbar: true },
                    _react2.default.createElement(
                        _reactstrap.Nav,
                        { className: 'ml-auto', navbar: true },
                        this.props.links.map(this.renderNavItem),
                        this.renderLogin()
                    )
                ),
                _react2.default.createElement(
                    _reactstrap.Modal,
                    { isOpen: this.state.modal, toggle: this.login, className: this.props.className },
                    _react2.default.createElement(
                        _reactstrap.ModalHeader,
                        { toggle: this.login },
                        'Login'
                    ),
                    _react2.default.createElement(
                        _reactstrap.ModalBody,
                        null,
                        _react2.default.createElement(_login2.default, { login: this.onLogin })
                    ),
                    _react2.default.createElement(
                        _reactstrap.ModalFooter,
                        null,
                        _react2.default.createElement(
                            _reactstrap.Button,
                            { color: 'secondary', onClick: this.login },
                            'Cancel'
                        )
                    )
                )
            );
        }
    }]);

    return Header;
}(_react.Component);

exports.default = Header;