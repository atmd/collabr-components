'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactstrap = require('reactstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Feedback = function (_Component) {
    _inherits(Feedback, _Component);

    function Feedback(props) {
        _classCallCheck(this, Feedback);

        var _this = _possibleConstructorReturn(this, (Feedback.__proto__ || Object.getPrototypeOf(Feedback)).call(this, props));

        _this.state = {
            modal: false
        };

        _this.toggle = _this.toggle.bind(_this);
        _this.onSubmit = _this.onSubmit.bind(_this);
        return _this;
    }

    _createClass(Feedback, [{
        key: 'toggle',
        value: function toggle() {
            this.setState({
                modal: !this.state.modal
            });
        }
    }, {
        key: 'onSubmit',
        value: function onSubmit() {
            this.props.onFeedback({
                email: document.getElementById('fb_email').value,
                message: document.getElementById('fb_message').value,
                newsletter: document.getElementById('fb_newsletter').checked
            });

            this.setState({
                modal: false
            });
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _reactstrap.Button,
                    { color: 'danger', onClick: this.toggle },
                    this.props.buttonLabel
                ),
                _react2.default.createElement(
                    _reactstrap.Modal,
                    { isOpen: this.state.modal, toggle: this.toggle, className: this.props.className },
                    _react2.default.createElement(
                        _reactstrap.ModalHeader,
                        { toggle: this.toggle },
                        'Feedback'
                    ),
                    _react2.default.createElement(
                        _reactstrap.ModalBody,
                        null,
                        _react2.default.createElement(
                            'p',
                            null,
                            'Lorem ipsum Excepteur sint ont mollit anim id est laborum.'
                        ),
                        _react2.default.createElement(
                            _reactstrap.Form,
                            null,
                            _react2.default.createElement(
                                _reactstrap.FormGroup,
                                null,
                                _react2.default.createElement(
                                    _reactstrap.Label,
                                    { 'for': 'exampleEmail' },
                                    'Email'
                                ),
                                _react2.default.createElement(_reactstrap.Input, { type: 'text', name: 'text', id: 'fb_email' })
                            ),
                            _react2.default.createElement(
                                _reactstrap.FormGroup,
                                null,
                                _react2.default.createElement(
                                    _reactstrap.Label,
                                    { 'for': 'exampleEmail' },
                                    'Email'
                                ),
                                _react2.default.createElement(_reactstrap.Input, { type: 'textarea', name: 'text', id: 'fb_message' })
                            ),
                            _react2.default.createElement(
                                _reactstrap.FormGroup,
                                { check: true },
                                _react2.default.createElement(
                                    _reactstrap.Label,
                                    { check: true },
                                    _react2.default.createElement(_reactstrap.Input, { type: 'checkbox', id: 'fb_newsletter' }),
                                    'Sign me up to the news letter'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _reactstrap.ModalFooter,
                        null,
                        _react2.default.createElement(
                            _reactstrap.Button,
                            { color: 'primary', onClick: this.onSubmit },
                            'submit'
                        ),
                        ' ',
                        _react2.default.createElement(
                            _reactstrap.Button,
                            { color: 'secondary', onClick: this.toggle },
                            'Cancel'
                        )
                    )
                )
            );
        }
    }]);

    return Feedback;
}(_react.Component);

exports.default = Feedback;