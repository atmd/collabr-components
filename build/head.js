"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactHelmet = require("react-helmet");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var site = _ref.site,
        subtitle = _ref.subtitle,
        url = _ref.url,
        description = _ref.description;

    return _react2.default.createElement(
        _reactHelmet.Helmet,
        null,
        _react2.default.createElement(
            "title",
            null,
            subtitle + " | " + site
        ),
        _react2.default.createElement("link", { rel: "canonical", href: url }),
        _react2.default.createElement("meta", { name: "description", content: description })
    );
};