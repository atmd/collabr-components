'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
    var picture = _ref.picture,
        name = _ref.name,
        nickname = _ref.nickname;

    return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement('img', { width: '64', style: { 'borderRadius': '200px' }, src: picture, alt: nickname }),
        _react2.default.createElement(
            'p',
            { style: { display: 'inline-block', 'marginLeft': '15px' } },
            name
        )
    );
};