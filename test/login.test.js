import React from 'react';
import {shallow, mount} from 'enzyme';

import { Button } from 'reactstrap';

import Component from '../src/login';

describe('login component', () => {

    let Login, eventHandler;
    const USERNAME = 'andrew@test.com';
    const PASSWORD = 'test123';

    describe('when rendered', () => {

        beforeEach(() => {
            eventHandler = jest.fn();
            Login = mount(<Component onLogin={eventHandler} />);
        });

        it('should show login controlls', () => {
            expect(Login.find('#l_username').length).toEqual(1)
            expect(Login.find('#l_password').length).toEqual(1)
        });

        describe('when clicking "login" without entering a username and passwords', () => {
            it('should not fire the login call back', () => {
                expect(eventHandler.mock.calls.length).toEqual(0);

                loginEntry(Login, '', '');

                expect(eventHandler.mock.calls.length).toEqual(0);
                // expect(eventHandler.mock.calls[0][0]).toEqual('andrew@test.com');
                // expect(eventHandler.mock.calls[0][1]).toEqual(PASSWORD);
            });
        });

        describe('when clicking "login" without entering a username and passwords', () => {
            it('should fire the login call back', () => {
                expect(eventHandler.mock.calls.length).toEqual(0);

                loginEntry(Login, USERNAME, PASSWORD);

                expect(eventHandler.mock.calls.length).toEqual(1);
                expect(eventHandler.mock.calls[0][0]).toEqual({email: USERNAME, password: PASSWORD});
            });
        });
    });
});


function loginEntry (form, user, pass) {
    form.find('#l_username').node.value = user;
    form.find('#l_password').node.value = pass;

    Object.defineProperty(document, 'getElementById', {
        value: function(selector) {
            if (selector == 'l_username') {
                return {value: user};
            } 
            return {value: pass}; 
        },
        configurable: true
    });

    form.find('button').simulate('click');
}
