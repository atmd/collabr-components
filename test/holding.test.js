import React from 'react';
import {shallow, mount} from 'enzyme';

import Component from '../src/holding';

describe('When rendering holding page', () => {
    describe('When a logo, brand name and subtitle is passed as props', () => {
        it('should render a holding page', () => {

            const IMAGE = 'http://placehold.it/30x30';
            const TITLE = 'some site';
            const SUBTITLE = 'coming soon';

            const holdingPage = mount(<Component logo={IMAGE} title={TITLE} subtitle={SUBTITLE} />);

            expect(holdingPage.find('.c-holding__logo').prop('src')).toEqual(IMAGE);
            expect(holdingPage.find('.c-holding__title').text()).toEqual(TITLE);
            expect(holdingPage.find('.c-holding__subtitle').text()).toEqual(SUBTITLE);
        });

        describe('When gived the newsletter prop', () => {
            it('should render newsletter form', () => {
            const holdingPage = mount(<Component logo={''} title={''} subtitle={''} newsletter />);
            expect(holdingPage.find('form').length).toEqual(1);
            });
        });
    });
});
